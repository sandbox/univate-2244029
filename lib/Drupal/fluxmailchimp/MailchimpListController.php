<?php

/**
 * @file
 * Contains MailchimpListController.
 */

namespace Drupal\fluxmailchimp;

use Drupal\fluxservice\Plugin\Entity\AccountInterface;
use Drupal\fluxservice\Plugin\Entity\ServiceInterface;
use Drupal\fluxservice\Entity\RemoteEntityControllerByAccount;
use Drupal\fluxservice\Entity\RemoteEntityInterface;

/**
 * Class 
 */
class MailchimpListController extends RemoteEntityControllerByAccount {

  /**
   * {@inheritdoc}
   */
  protected function loadFromService($ids, ServiceInterface $service, AccountInterface $account) {
    $output = array();
    $client = $account->client();

    foreach (array_chunk($ids, 100) as $group) {
      $params = array(
        'filters' => array(
          'list_id' => (string) implode(',', $group),
          'exact' => false,
        ),
        'limit' => 100,
      );
      if ($response = $client->getLists($params)) {
        foreach ($response['data'] as $list) {
          $output[$list['id']] = $list;
        }
      }
    }

    return $output;
  }

  protected function sendToService(RemoteEntityInterface $entity) {
    throw new \Exception("The entity type {$this->entityType} does not support writing.");
  }

  /**
   * {@inheritdoc}
   */
  protected function preEntify(array &$items, ServiceInterface $service, AccountInterface $account = NULL) {
    foreach ($items as $item) {
      $lists[$item['id']] = $item;
    }
    $items = $lists;
  }

}
