<?php

/**
 * @file
 * Contains MailchimpHomeTimelineTaskHandler.
 */

namespace Drupal\fluxmailchimp\TaskHandler;

/**
 * Event dispatcher for the Mailchimp home timeline of a given user.
 */
class MailchimpAccountTaskHandler extends MailchimpTaskHandlerBase {

  /**
   * {@inheritdoc}
   */
  protected function getAccount(array $arguments) {
    $account = $this->getAccount();
    $tweets = array();
    if ($response = $account->client()->getHomeTimeline($arguments)) {
      $tweets = fluxservice_entify_multiple($response, 'fluxmailchimp_account', $account);
    };
    // Mailchimp sends the Tweets in the wrong order.
    return array_reverse($tweets);
  }

}
