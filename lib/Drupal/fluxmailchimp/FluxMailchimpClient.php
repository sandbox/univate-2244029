<?php

/**
 * @file
 * Contains FluxMailchimpClient.
 */

namespace Drupal\fluxmailchimp;

use Drupal\fluxservice\ServiceClientInterface;
use Guzzle\Common\Collection;
use Guzzle\Plugin\Mock\MockPlugin;
use Guzzle\Http\Message\Response;
use ZfrMailChimp\Client\MailChimpClient;

/**
 * Guzzle driven service client for the Mailchimp API.
 */
class FluxMailchimpClient extends MailchimpClient {

  protected $mock;

  /**
   * {@inheritdoc}
   */
  public static function factory($config = array()) {
    $apiKey = implode('-', array($config['access_token'], $config['dc']));
    $client = new static($apiKey);

    return $client;
  }

  public function enableMock() {
    $this->mock = new MockPlugin();

    // Add the mock plugin to the client object
    $this->addSubscriber($this->mock);
  }

  public function mockResponse($response) {
    $this->mock->addResponse($response);
  }
}
