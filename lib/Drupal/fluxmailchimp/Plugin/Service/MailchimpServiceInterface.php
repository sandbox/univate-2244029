<?php

/**
 * @file
 * Contains MailchimpServiceInterface
 */

namespace Drupal\fluxmailchimp\Plugin\Service;

use Drupal\fluxservice\Service\OAuthServiceInterface;

/**
 * Interface for Mailchimp services.
 */
interface MailchimpServiceInterface extends OAuthServiceInterface {

}