<?php

/**
 * @file
 * Contains MailchimpCampaign.
 */

namespace Drupal\fluxmailchimp\Plugin\Entity;

use Drupal\fluxservice\Entity\FluxEntityInterface;
use Drupal\fluxservice\Entity\RemoteEntity;

/**
 * Entity class for Mailchimp Campaigns.
 */
class MailchimpCampaign extends RemoteEntity {

  /**
   * Defines the entity type.
   *
   * This gets exposed to hook_entity_info() via fluxservice_entity_info().
   */
  public static function getInfo() {
    return array(
      'name' => 'fluxmailchimp_campaign',
      'label' => t('Mailchimp: Campaign'),
      'module' => 'fluxmailchimp',
      'service' => 'fluxmailchimp',
      'controller class' => '\Drupal\fluxmailchimp\MailchimpCampaignController',
      'label callback' => 'entity_class_label',
      'entity keys' => array(
        'id' => 'drupal_entity_id',
        'remote id' => 'id',
      ),
    );
  }

  /**
   * Gets the entity property definitions.
   */
  public static function getEntityPropertyInfo($entity_type, $entity_info) {
    $info['id'] = array(
      'label' => t('Remote identifier'),
      'description' => t('The unique remote identifier of the Campaign.'),
      'type' => 'text',
    );

    $info['web_id'] = array(
      'label' => t('Web Id'),
      'description' => t('The campaign id used in our web app, allows you to create a link directly to it.'),
      'type' => 'integer',
    );

    $info['list_id'] = array(
      'label' => t('List Id'),
      'description' => t('The List used for this campaign.'),
      'type' => 'text',
    );

    $info['title'] = array(
      'label' => t('Title'),
      'description' => t('The title of the campaign.'),
      'type' => 'text',
    );

    $info['type'] = array(
      'label' => t('Type'),
      'description' => t('The type of campaign this is (regular,plaintext,absplit,rss,inspection,auto)'),
      'type' => 'text',
    );

    $info['create_time'] = array(
      'label' => t('Creation Time'),
      'description' => t('The creation time for the campaign.'),
      'type' => 'date',
      'getter callback' => 'fluxservice_entity_property_getter_method',
    );

    $info['send_time'] = array(
      'label' => t('Send Time'),
      'description' => t('The send time for the campaign - also the scheduled time for scheduled campaigns.'),
      'type' => 'date',
      'getter callback' => 'fluxservice_entity_property_getter_method',
    );

    $info['emails_sent'] = array(
      'label' => t('Web Id'),
      'description' => t('The campaign id used in our web app, allows you to create a link directly to it.'),
      'type' => 'integer',
    );

    $info['status'] = array(
      'label' => t('Status'),
      'description' => t('The status of the given campaign (save,paused,schedule,sending,sent).'),
      'type' => 'text',
    );

    $info['from_name'] = array(
      'label' => t('From Name'),
      'description' => t('From name of the given campaign'),
      'type' => 'text',
    );

    $info['from_email'] = array(
      'label' => t('From Email'),
      'description' => t('Reply-to email of the given campaign'),
      'type' => 'text',
    );

    $info['subject'] = array(
      'label' => t('Subject'),
      'description' => t('Subject of the given campaign'),
      'type' => 'text',
    );

    return $info;
  }

  /**
   * The title of the campaign.
   *
   * @var string
   */
  public $title;

  /**
   * The status of the given campaign (save,paused,schedule,sending,sent).
   *
   * @var string
   */
  public $status;

  /**
   * The status of the given campaign (save,paused,schedule,sending,sent).
   *
   * @var string
   */
  public $send_time;

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus() {
    return $this->status;
  }

  /**
   * {@inheritdoc}
   */
  public function getSendTime() {
    return $this->send_time;
  }

}
