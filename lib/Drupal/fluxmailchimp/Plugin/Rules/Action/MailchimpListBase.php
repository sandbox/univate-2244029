<?php

/**
 * @file
 * Contains MailchimpSubscribe.
 */

namespace Drupal\fluxmailchimp\Plugin\Rules\Action;

//use Drupal\fluxmailchimp\Plugin\Entity\MailchimpList;
//use Drupal\fluxmailchimp\Plugin\Entity\MailchimpListInterface;
use Drupal\fluxmailchimp\Plugin\Service\MailchimpAccountInterface;
use Drupal\fluxmailchimp\Rules\RulesPluginHandlerBase;

/**
 * "Add a user to list" action.
 */
abstract class MailchimpListBase extends RulesPluginHandlerBase implements \RulesActionHandlerInterface {

  /**
   * Rules 'options list' callback for retrieving a Mailchimp list options.
   */
  public static function getListOptions($element, $name = NULL) {
    $options = array();
    if (!empty($element->settings['account']) && $account = entity_load_single('fluxservice_account', $element->settings['account'])) {
      $lists = $account->client()->getLists();
      $lists = fluxservice_entify_multiple($lists['data'], 'fluxmailchimp_list', $account);

      foreach ($lists as $list) {
        $identifier = $list->getRemoteIdentifier();
        $name = $list->getName();
        $options[$identifier] = $name;
      }
    }
    return $options;
  }

  /**
   * Rules 'options list' callback for retrieving a Mailchimp list options.
   */
  public static function getFieldOptions($bundle = 'personal', $type = NULL) {
    $options = array();

    // load all the profile2 fields
    $fields = field_info_instances('profile2', $bundle);
    foreach ($fields as $name => $field) {
      if ($type == NULL || in_array($field['widget']['type'], array('options_select'))) {
        $options[$name] = $field['label'] . '<!--(' . $field['widget']['type'] . ')-->';
      }
    }
    return $options;
  }

  /**
   * @param $account
   * @param $list
   * @param $interests
   * @return mixed
   */
  public static function updateInterestGroups($account, $list, $interests) {
    $interest_values  = fluxservice_key_value("{$account->plugin}.lists");
    $id = $account->remote_id . ':' . $list;
    $params = $interest_values->get($id);
    if (empty($params)) {
      $params = array();
    }

    foreach ($interests as $interest) {
      if (empty($interest)) continue;

      $field = field_info_field($interest);
      $values = list_allowed_values($field);

      self::updateInterestGroupAllowedValues($account, $list, $interest, $values);

      $params['groups'][$interest] = array_keys($values);
    }

    $interest_values->set($id, $params);

    return $params['groups'];
  }

  /**
   * @param $account
   * @param $list
   * @param $interest
   * @param $values
   */
  public static function updateInterestGroupAllowedValues($account, $list, $interest, $values) {
    static $current_groups;

    if (!isset($current_groups[$list])) {
      try {
        $args = array('id' => $list);
        $current_groups[$list] = $account->client()->getInterestGroupings($args);
      }
      catch (\ZfrMailChimp\Exception\Ls\InvalidOptionException $e) {
        $current_groups[$list] = array();
      }
      catch (\Guzzle\Http\Exception\ServerErrorResponseException $e) {
        $current_groups[$list] = array();
      }
    }

    $interest_exists = FALSE;
    foreach ($current_groups[$list] as $group) {
      if ($interest == $group['name']) {
        // @todo - need to check the values are still all there.
        foreach ($values as $value => $label) {
          $found = FALSE;
          foreach ($group['groups'] as $item) {
            if ($item['name'] == $value) {
              $found = TRUE;
              break;
            }
          }
          if (!$found) {
            $args = array(
              'id' => $list,
              'group_name' => $value,
              'grouping_id' => $group['id']
            );
            $account->client()->addInterestGroup($args);
          }
        }
        $interest_exists = TRUE;
        break;
      }
    }
    if (!$interest_exists) {
      $args = array(
        'id' => $list,
        'name' => $interest,
        'type' => 'dropdown',
        'groups' => array_keys($values),
      );
      $account->client()->addInterestGrouping($args);
    }
  }

  public static function getMergeVarTag($field_name) {
    $field_name = str_replace('field_', '', $field_name);
    $field_name = strtoupper(substr($field_name, 0, 10));
    return $field_name;
  }

  /**
   * @param $account
   * @param $list
   * @param $marge_vars
   */
  public static function updateMergeVars($account, $list, $merge_vars) {
    $interest_values  = fluxservice_key_value("{$account->plugin}.list");
    $id = $account->remote_id . ':' . $list;
    $params = $interest_values->get($id);
    if (empty($params)) {
      $params = array();
    }

    foreach ($merge_vars as $var => $tag) {
      if (empty($tag)) continue;

      //$field = field_info_field($var);
      $tag = self::getMergeVarTag($tag);
      self::updateMergeVar($account, $list, $tag, $var);

      $params['merge_var'][$tag] = $var;
    }

    $interest_values->set($id, $params);

    return $params['merge_var'];
  }

  public static function updateMergeVar($account, $list, $tag, $name) {
    static $current_vars;
    if (!isset($current_vars[$list])) {
      try {
        $args = array('id' => array($list));
        $current_vars[$list] = $account->client()->getListMergeVars($args);
      }
      catch (\ZfrMailChimp\Exception\Ls\InvalidOptionException $e) {
        $current_vars[$list] = array();
      }
      catch (\Guzzle\Http\Exception\ServerErrorResponseException $e) {
        $current_vars[$list] = array();
      }
    }

    $var_exists = FALSE;
    foreach ($current_vars[$list]['data'][0]['merge_vars'] as $merge_var) {
      if (empty($merge_var)) continue;

      if ($merge_var['tag'] == $tag) {
        $var_exists = TRUE;
      }
    }

    if (!$var_exists) {
      $params = array(
        'id' => $list,
        'tag' => $tag,
        'name' => $name,
        'options' => array(
          'field_type' => 'text' // @todo - hardcoded to text - support other types later.
        ),
      );
      $account->client()->addListMergeVar($params);
    }
  }
}